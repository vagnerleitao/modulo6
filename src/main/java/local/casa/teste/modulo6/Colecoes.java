/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package local.casa.teste.modulo6;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author vagner
 */
public class Colecoes {
    private int numero;
    private String nome;

    
    public static void main(String[] args){
        
        

        Colecoes valor = new Colecoes(0, null);
        valor.Exemplo04();
        
    }
    
   
    
    public void Exemplo06(){
        ArrayDeque<Colecoes> time = new ArrayDeque<>();
        time.add(new Colecoes(4, "David Luiz"));
        time.addFirst(new Colecoes(10, "Neymar"));
        time.addLast(new Colecoes(12, "Júlio César"));

        Iterator<Colecoes> it = time.iterator();
        while (it.hasNext()) {
            Colecoes j = it.next();
            System.out.println(j);
        }
    }
    
    public void Exemplo05(){
        HashMap<String, Colecoes> time = new HashMap<>();
        time.put("Goleiro", new Colecoes(12, "Júlio César"));
        time.put("Zagueiro", new Colecoes(4, "David Luiz"));
        time.put("Atacante", new Colecoes(10, "Neymar"));
    
        System.out.println("Escalação:");
        Set<Map.Entry<String, Colecoes>> entries = time.entrySet();
        for (Map.Entry<String, Colecoes> entry: entries) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
    
    public void Exemplo04(){
        HashMap<String, Colecoes> time = new HashMap<>();
        time.put("Goleiro", new Colecoes(12, "Júlio César"));
        time.put("Zagueiro", new Colecoes(4, "David Luiz"));
        time.put("Atacante", new Colecoes(10, "Neymar"));
    
        System.out.println("Escalação:");
        System.out.println(time.get("Goleiro"));
        System.out.println(time.get("Zagueiro"));
        System.out.println(time.get("Atacante"));
    }
    
    
    public void Exemplo03(){
        ArrayDeque<Colecoes> time = new ArrayDeque<>();
        time.add(new Colecoes(4, "David Luiz"));
        time.addFirst(new Colecoes(10, "Neymar"));
        time.addLast(new Colecoes(12, "Júlio César"));

        //for(int j=0;j < time.size();j++){
        for (Colecoes j: time) {
            System.out.println(j);
        }
    }


    
    public void Exemplo02(){
        ArrayList<Colecoes> time = new ArrayList<>();    
        time.add(new Colecoes(4, "David Luiz"));
        time.add(new Colecoes(10, "Neymar"));
        time.add(new Colecoes(12, "Júlio César"));
        for (Colecoes j: time) {
            System.out.println(j);
        }
    }
    
    
    private Colecoes(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return numero + " - " + nome;
    }
    
    
    public void Exemplo01(){
        ArrayList<String> valores = new ArrayList<>();
        valores.add("Vagner");
        valores.add("Goncalves");
        valores.add("Leitao");
        int soma = 0;
        for (String n: valores) {
            soma = soma+1;
            System.out.println("Total = "+valores.get(soma-1));
        }
    }
    
}
